package com.mgmtp.service.helloworld;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "message")
public final class Message {

	private final String text;

	Message(String message) {
		this.text = message;
	}

	public String getText() {
		return text;
	}
}
