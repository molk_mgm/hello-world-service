package com.mgmtp.service.helloworld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController("/")
public final class HelloWorldEndpoint {

	@RequestMapping(
		method = GET,
		path = "hello-world",
		produces = {"application/xml;charset=UTF-8", "application/json;charset=UTF-8"})
	public static Message helloWorld() {
		return new Message(hostInfo);
	}

	// internal helpers -----------------------------------------------------------------------------

	private static String osInfo   = syspropsFor("os.name", "os.version", "os.arch");
	private static String javaInfo = syspropsFor("java.version", "java.vendor");
	private static String hostInfo = format("Hello world from %s (Java %s), on %s", osInfo, javaInfo, hostname());

	private static String syspropsFor(String... keys) {
		return stream(keys).map(System::getProperty).collect(joining(" "));
	}

	private static String hostname() {
		try {
			return InetAddress.getLocalHost().toString();
		}
		catch (Exception ignored) {
			return "n.a.";
		}
	}

}
