package com.mgmtp.service.helloworld

import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.util.LinkedMultiValueMap
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ContextConfiguration(classes = Application)
@SpringBootTest(webEnvironment = RANDOM_PORT)
class ApplicationTest extends Specification {

	@Autowired
	private TestRestTemplate restTemplate

	def 'ask for XML' () {
		setup:
		def response = sendRequest HttpMethod.GET, '/hello-world', [Accept: 'application/xml']

		expect:
		response
		response.statusCode == HttpStatus.OK
		response.hasBody()
		(parseXml(response.body).text as String)?.contains 'Hello world from'
	}

	def 'ask for JSON' () {
		setup:
		def response = sendRequest HttpMethod.GET, '/hello-world', [Accept: 'application/json']

		expect:
		response
		response.statusCode == HttpStatus.OK
		response.hasBody()
		(parseJson(response.body).text as String)?.contains 'Hello world from'
	}

	@Unroll('asking for "#mimeType" -> #expectedStatusCode')
	def 'GET on hello-world service accepting mime type' () {
		setup:
		def response = sendRequest HttpMethod.GET, '/hello-world', [Accept: mimeType]

		expect:
		response
		response.statusCode == expectedStatusCode

		where:
		mimeType			|| expectedStatusCode
		'application/xml'	|| HttpStatus.OK
		'application/json'	|| HttpStatus.OK
		'text/plain'		|| HttpStatus.NOT_ACCEPTABLE
	}

	def 'POST on hello world service' () {
		setup:
		def response = sendRequest HttpMethod.POST, '/hello-world'

		expect:
		response
		response.statusCode == HttpStatus.METHOD_NOT_ALLOWED
	}

	ResponseEntity<String> sendRequest(HttpMethod httpMethod, String url, Map httpHeaders = [:]) {
		httpHeaders = new LinkedMultiValueMap(httpHeaders.collectEntries([:]) { k, v -> [(k): [v]]})

		restTemplate.exchange(url, httpMethod, new HttpEntity(httpHeaders), String)
	}

	static parseXml(String text) {
		new XmlSlurper().parseText text
	}

	static parseJson(String text) {
		new JsonSlurper().parseText text
	}
}
