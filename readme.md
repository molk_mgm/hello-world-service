# 'Hello World' Service

This project builds a simple _hello world_ service exposing a single HTTP service endpoint `hello-world` on port `4223`.

It is based on the [SpringBoot](https://spring.io/guides/gs/spring-boot/) framework.


## How to build it

The service application is a single JAR file built with [Maven](https://maven.apache.org/).

To build the JAR file run the following command

    $ mvn package


## How to run it

To run the service simply pass it to the Java 8 runtime executable:

    $ java -jar target/hello-world-*.jar

Once the service is up, the one and only endpoint can be called:
 
    $ curl localhost:4223/hello-world

The result returned in the HTTP body should be something like this:

    <message>
      <text>Hello world from Mac OS X 10.12.6 x86_64 (Java 1.8.0_144 Oracle Corporation), on marcusonb/10.15.1.59</text>
    </message>

The service returns the body in XML format by default and offers JSON when asking for it:

    $ curl -H 'Accept: application/json' localhost:4223/hello-world
    {
      "text" : "Hello world from Mac OS X 10.12.6 x86_64 (Java 1.8.0_144 Oracle Corporation), on marcusonb/10.15.1.59"
    }

### Running it in a Docker container

There are two Docker files for bundling the service into a Docker container:

1. [`Dockerfile-alpine`](Dockerfile-alpine) extending [Alpine](https://hub.docker.com/r/anapsix/alpine-java/) and
2. [`Dockerfile-centos`](Dockerfile-centos) extending [Centos](https://hub.docker.com/_/centos/)

To run the service in a container based on the Alpine image, run the following commands:

    $ docker build -t hello-world-alpine:latest -f Dockerfile-alpine .
    $ docker create --name hello-world-alpine -p 4223:4223 hello-world-alpine
    $ docker start hello-world-alpine
 
As the container port has been published to `localhost` the service call should give you something like this now:

    <message>
      <text>Hello world from Linux 4.9.49-moby amd64 (Java 1.8.0_144 Oracle Corporation), on 14e11dc902b7/172.17.0.2</text>
    </message>

Running in a Centos based container should give you someting like this:

    <message>
      <text>Hello world from Linux 4.9.49-moby amd64 (Java 1.8.0_151 Oracle Corporation), on 1eb7a4f0d4e6/172.17.0.3</text>
    </message>
